<?php
/* @var $this yii\web\View */
/* @var $apples[] backend\models\Apple */

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="container">
<h1>Apples</h1>
    <a href="<?= Url::to(['apple/generate']); ?>" class="btn btn-success">Generate apples</a>
    <a href="<?= Url::to(['apple/delete-all']); ?>" class="btn btn-danger" style="margin-left: 30px">Delete all apples</a>

    <div class="row">
        <?php if($apples): ?>
            <?php foreach ($apples as $apple): ?>
                <div class="col-sm-4">
                    <h3><?= $apple->color; ?> apple</h3>
                    <p><?= $apple->getIntSize(); ?></p>
                    <p>status: <?= $apple->getStatus(); ?></p>
                    <a href="<?= Url::to(['apple/view', 'id' => $apple->id]); ?>">View</a>
                    <?php if(!$apple->isFell() && !$apple->isSpoiled()): ?>
                        <a href="<?= Url::to(['apple/fall', 'id' => $apple->id]); ?>">Fall</a>
                    <?php endif; ?>
                    <a href="<?= Url::to(['apple/delete', 'id' => $apple->id]); ?>">Delete</a>
                    <?php $form = ActiveForm::begin([
                        'action' => ['apple/eat', 'id' => $apple->id],
                        'method' => 'post'
                    ]); ?>
                        <?= $form->field($appleForm, 'percent')->input('number', ['min' => 0, 'max' => $apple->getIntSize()]); ?>
                        <?= Html::submitButton('eat', ['class' => 'btn btn-success']); ?>
                    <?php ActiveForm::end(); ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <h2>Яблок нет</h2>
        <?php endif; ?>
    </div>    
</div>
