<?php

namespace backend\models;

use backend\models\exceptions\StatusException;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property string $color
 * @property string $birth_date
 * @property string|null $date_of_fall
 * @property float $size
 * @property string $status
 */
class Apple extends \yii\db\ActiveRecord
{
    
    public const ON_TREE = 'apple on tree';
    public const FELL = 'the apple fell to the ground';
    public const SPOILED = 'the apple is spoiled';
    
    public const SPOILED_TIME = 18000;
    
    public function __construct($config = array())
    {
        $this->status = self::ON_TREE;
        parent::__construct($config);
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color', 'birth_date', 'size', 'status'], 'required'],
            [['birth_date', 'date_of_fall'], 'safe'],
            [['size'], 'number'],
            [['color', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
            'birth_date' => 'Birth Date',
            'date_of_fall' => 'Date Of Fall',
            'size' => 'Size',
            'status' => 'Status',
        ];
    }
    
    public function eat(int $percent)
    {
        if($this->isFell()) {
            $p = (double) $percent / 100;
            if($p >= $this->size) {
                $this->delete();
            } else {
                $this->size -= $p;
                $this->save();
            }
        } else {
            throw new StatusException('The apple is still on the tree or spoiled.');
        }
    }
    
    public function spoil()
    {
        if($this->isFell() && !$this->isSpoiled()) {
            $this->status = self::SPOILED;
            $this->save();
        } else {
            throw new StatusException('The apple is still on the tree.');
        }
    }
    
    public function getIntSize()
    {
        return $this->size * 100;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function fallToGround()
    {
        $this->status = self::FELL;
        $this->date_of_fall = time();
        return $this->save();
    }
    
    public function isSpoiled(): bool
    {
        return $this->status === self::SPOILED;
    }
    
    public function isOnTree(): bool
    {
        return $this->status === self::ON_TREE;
    }
    
    public function isFell(): bool
    {
        return $this->status === self::FELL;
    }
    
}
