<?php

namespace backend\helpers;

/**
 * Description of ColorHelper
 *
 * @author grigo
 */
class ColorHelper
{
    
    public static function generateRandomColor()
    {
        $number = random_int(1, 5);
        switch($number) {
            case 1:
                $color = 'red';
                break;
            case 2:
                $color = 'green';
                break;
            case 3:
                $color = 'yellow';
            case 4:
                $color = 'golden';
            default:
                $color = 'rose';
        }
        return $color;
    }
}
