<?php

namespace backend\models\forms;

use yii\base\Model;

use backend\models\queries\AppleQuery;

/**
 * Description of AppleEatingForm
 *
 * @author grigo
 */
class AppleEatingForm extends Model
{
    
    public $percent;
    
    public function rules()
    {
        return [
            ['percent', 'required'],
            ['percent', 'number']
        ];
    }
    
    public function eat(int $id)
    {
        if($this->validate()) {
            $apple = AppleQuery::getAppleById($id);
            $apple->eat($this->percent);
            return true;
        }
    }
}
