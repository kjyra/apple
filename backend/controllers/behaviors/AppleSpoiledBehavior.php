<?php

namespace backend\controllers\behaviors;

use yii\base\Behavior;
use backend\models\queries\AppleQuery;
use yii\web\Controller;

/**
 * Description of AppleSpoiledBehavior
 *
 * @author grigo
 */
class AppleSpoiledBehavior extends  Behavior
{
    
    public function events(): array
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'checkApples'
        ];
    }
    
    public function checkApples()
    {
        $apples = AppleQuery::findNotSpoiledApples();
        if($apples) {
            foreach($apples as $apple) {
                $apple->spoil();
            }
        }
    }
}
