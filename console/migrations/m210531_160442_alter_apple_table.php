<?php

use yii\db\Migration;

/**
 * Class m210531_160442_alter_apple_table
 */
class m210531_160442_alter_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%apple%}}', 'birth_date');
        $this->dropColumn('{{%apple%}}', 'date_of_fall');
        $this->addColumn('{{%apple%}}', 'birth_date', $this->integer()->notNull());
        $this->addColumn('{{%apple%}}', 'date_of_fall', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%apple%}}', 'birth_date');
        $this->dropColumn('{{%apple%}}', 'date_of_fall');
        $this->addColumn('{{%apple%}}', 'birth_date', $this->timestamp()->notNull());
        $this->addColumn('{{%apple%}}', 'date_of_fall', $this->timestamp());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210531_160442_alter_apple_table cannot be reverted.\n";

        return false;
    }
    */
}
