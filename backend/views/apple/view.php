<?php
/* @var $this yii\web\View */
/* @var $apple frontend\models\Apple */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<h1>Apples</h1>

<div class="container">
    <div class="row">
        <div class="col-12">
            <h3><?= $apple->color; ?> apple</h3>
            <p><?= $apple->getIntSize(); ?></p>
            <p>status: <?= $apple->getStatus(); ?></p>
            <a href="<?= Url::to(['apple/index']); ?>">All apples</a>
            <?php if(!$apple->isFell() && !$apple->isSpoiled()): ?>
                <a href="<?= Url::to(['apple/fall', 'id' => $apple->id]); ?>">Fall</a>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin([
                        'action' => ['apple/eat', 'id' => $apple->id],
                        'method' => 'post'
            ]);
            ?>
            <?= $form->field($appleForm, 'percent')->input('number', ['min' => 0, 'max' => $apple->getIntSize()]); ?>
<?= Html::submitButton('eat', ['class' => 'btn btn-success']); ?>
<?php ActiveForm::end(); ?>
        </div>
    </div>    
</div>
