<?php

namespace backend\controllers;

use Yii;
use backend\models\queries\AppleQuery;
use backend\models\forms\AppleEatingForm;
use backend\controllers\behaviors\AppleSpoiledBehavior;
use backend\controllers\behaviors\AccessBehavior;
use backend\models\Apple;
use backend\helpers\ColorHelper;

class AppleController extends \yii\web\Controller
{
    
    public function behaviors(): array
    {
        return [
            AppleSpoiledBehavior::class,
            AccessBehavior::class,
        ];
    }
    
    public function actionIndex()
    {
        $appleForm = new AppleEatingForm();
        $apples = AppleQuery::getAll();
        return $this->render('index', [
            'apples' => $apples,
            'appleForm' => $appleForm,
        ]);
    }
    
    public function actionView(int $id)
    {
        $appleForm = new AppleEatingForm();
        $apple = AppleQuery::getAppleById($id);
        
        return $this->render('view', [
            'apple' => $apple,
            'appleForm' => $appleForm,
            'p' => $p
        ]);
    }
    
    public function actionDelete(int $id)
    {
        $apple = AppleQuery::getAppleById($id);
        $apple->delete();
        return $this->redirect(['apple/index']);
    }
    
    public function actionDeleteAll()
    {
        Apple::deleteAll();
        return $this->redirect(['apple/index']);
    }
    
    public function actionFall(int $id)
    {
        $apple = AppleQuery::getAppleById($id);
        if($apple->fallToGround()) {
            return $this->redirect(['apple/index']);
        }
    }
    
    public function actionEat(int $id)
    {
        $appleForm = new AppleEatingForm();
        if(Yii::$app->request->post()) {
            if($appleForm->load(Yii::$app->request->post()) && $appleForm->eat($id)) {
                return $this->redirect(['apple/index']);
            }
        }
    }
    
    public function actionGenerate()
    {
        $count = random_int(2, 8);
        
        for($i = 0; $i < $count; $i++) {
            $apple = new Apple([
                'color' => ColorHelper::generateRandomColor(),
                'status' => Apple::ON_TREE,
                'birth_date' => strtotime('yesterday') - random_int(0, 500000),
                'date_of_fall' => null,
                'size' => 1.00
            ]);
            $apple->save();
        }
        return $this->redirect(['apple/index']);
    }

}
