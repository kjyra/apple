<?php

namespace backend\models\queries;

use backend\models\Apple;
use backend\models\exceptions\AppleNotFoundException;

/**
 * Description of AppleQuery
 *
 * @author grigo
 */
class AppleQuery
{
    
    public static function getAll(): ?array
    {
        if($apples = Apple::find()->all()) {
            return $apples;
        }
        return null;
    }
    
    public static function getAppleById(int $id): Apple
    {
        if($apple = Apple::find()->where(['id' => $id])->one()) {
            return $apple;
        }
        throw new AppleNotFoundException('no such apple exists');
    }
    
    public static function findNotSpoiledApples(): ?array
    {
        $now = time() - Apple::SPOILED_TIME;
        if($apples = Apple::find()->where(['status' => Apple::FELL])->andWhere('date_of_fall < :now', [':now' => $now])->all()) {
            return $apples;
        }
        return null;
    }
}
