<?php

use yii\db\Migration;

/**
 * Class m210531_165248_alter_apple_table_size
 */
class m210531_165248_alter_apple_table_size extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%apple%}}', 'size');
        $this->addColumn('{{%apple%}}', 'size', $this->decimal(5,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%apple%}}', 'size');
        $this->addColumn('{{%apple%}}', 'size', $this->decimal());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210531_165248_alter_apple_table_size cannot be reverted.\n";

        return false;
    }
    */
}
